from Crud import Crud
from flask import Flask, render_template, request, url_for, redirect


crud = Crud("ec2-52-45-238-24.compute-1.amazonaws.com","de0k66p3bcferp","atweqkvkbmrwrc","6b6576d8ab52d49c7fc2d5e9642b297442089bd5c5a385c6db1a69cc81deed09")

app = Flask(__name__)

#@app.route('/leer_clientes')
#def leerClientes():
#    clientes_tienda = crud.leer_clientes()
#    return str(clientes_tienda)

@app.route('/inicio')
def inicio():
    return render_template('inicio.html')

@app.route('/carrito')
def carrito():
    return render_template('carrito_compras.html')

@app.route('/papas')
def papas():
    return render_template('papas.html')

@app.route('/vinzela')
def vinzela():
    return render_template('vinzela.html')

@app.route('/pasta')
def pasta():
    return render_template('pasta.html')

@app.route('/arroz')
def arroz():
    return render_template('arroz.html')

@app.route('/agua')
def agua():
    return render_template('agua.html')

@app.route('/condon')
def condon():
    return render_template('condon.html')

@app.route('/mante')
def mante():
    return render_template('mante.html')

@app.route('/pan')
def pan():
    return render_template('pan.html')

@app.route('/aceite')
def aceite():
    return render_template('aceite.html')

@app.route('/registro')
def registro():
    return render_template('registro.html')

@app.route('/')
def login():
    return render_template('login.html')

@app.route('/insertarclientes', methods=['POST'])
def insertarCliente():
    if request.method=='POST':
        nombre=request.form['nombre']
        documento=request.form['documento']
        email=request.form['email'] 
        direccion=request.form['direccion']
        numero_contacto=request.form['numero']
        ciudad=request.form['ciudad']
        rol= str(3)
        contraseña=request.form['pass']
        nuevo_usuario=crud.insertar_cliente(nombre, documento, email, direccion ,numero_contacto, ciudad, rol)
        nueva_credencial=crud.insertar_credencial(email,contraseña,rol)
        return str (nuevo_usuario) and str (nueva_credencial) #deveulve el none en el html
        #print (nombre, documento, email, direccion ,numero_contacto, ciudad, rol)
    return render_template('inicio.html')

'''    
@app.route('/login', methods=['POST'])
def login():
    if request.method=='POST':
        email=request.form['email'] 
        contraseña=request.form['pass']
        nuevo_usuario=crud.insertar_cliente(nombre, documento, email, direccion ,numero_contacto, ciudad, rol)
        nueva_credencial=crud.insertar_credencial(email,contraseña,rol)
        return str (nuevo_usuario) and str (nueva_credencial) #deveulve el none en el html
        #print (nombre, documento, email, direccion ,numero_contacto, ciudad, rol)
    return render_template('inicio.html')
'''

if __name__=="__main__":
    print("arrancando servidor...")
    app.run(debug=True,host="0.0.0.0")