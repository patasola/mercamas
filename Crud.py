from Conexion_db import Conexion_db


class Crud:

    def __init__(self, mi_host, database, user, passwd):
        self.conn = Conexion_db(mi_host, database, user, passwd)

    def leer_clientes(self):
        query = 'SELECT * from "Cliente"'
        clientes = self.conn.consultar_db(query)
        return clientes

    def leer_clave(self):
        query = 'SELECT * from "Cliente" INNER JOIN "Credenciales" ON usr = correo'
        clientes = self.conn.consultar_db(query)
        return clientes

    def leer_credenciales(self):
        query = 'SELECT * from "Credenciales"'
        credenciales = self.conn.consultar_db(query)
        return credenciales

    def leer_detalleCompras(self):
        query = 'SELECT * from "Detalle_Compra"'
        compras = self.conn.consultar_db(query)
        return compras

    def leer_detalleVentas(self):
        query = 'SELECT * from "Detalle_Ventas"'
        ventas = self.conn.consultar_db(query)
        return ventas

    def leer_productos(self):
        query = 'SELECT * from "Productos"'
        productos = self.conn.consultar_db(query)
        return productos

    def leer_proveedor(self):
        query = 'SELECT * from "Proveedor"'
        proveedor = self.conn.consultar_db(query)
        return proveedor

    def leer_tendero(self):
        query = 'SELECT * from "Tendero"'
        tendero = self.conn.consultar_db(query)
        return tendero

    def insertar_cliente(self, nombre, documento, correo, direccion, numero_contacto, ciudad, rol):
        query = 'INSERT INTO "Cliente" (nombre, documento, correo, direccion, numero_contacto, ciudad, rol)' +\
            "VALUES ('"+nombre+"','"+documento+"','"+correo+"','" + \
                     direccion+"','"+numero_contacto+"','"+ciudad+"','"+rol+"')"
        self.conn.escribir_db(query)

    def eliminar_cliente(self, id):
        query = "DELETE FROM \"Cliente\"" +\
            "WHERE id="+str(id)
        self.conn.escribir_db(query)
    
    def actualizar_cliente(self,id, nombre, documento, correo, direccion, numero_contacto, ciudad, rol):
       # print(id, nombre, documento, correo, direccion, numero_contacto, ciudad, rol)
        query= "UPDATE \"Cliente\" SET "+ "nombre="+nombre+ ", documento="+documento +\
             ",correo="+correo+", direccion="+direccion+\
              ",numero_contacto="+numero_contacto+", ciudad="+ciudad+\
               ", rol="+rol+\
                " WHERE id="+id+";"
        self.conn.escribir_db(query)    
    
    def actualizar_producto(self,id, nombre, categoria, descripcion, foto):
        query= "UPDATE \"Productos\" SET "+ "nombre="+nombre+ ", categoria="+categoria +\
             ",descripcion="+descripcion+", foto="+foto+\
                " WHERE id="+id+";"
        self.conn.escribir_db(query)               
    
    def insertar_producto(self,nombre, categoria, descripcion, foto):
        query = 'INSERT INTO "Productos" (nombre, categoria, descripcion, foto)'+\
            "VALUES ('"+nombre+"','"+categoria+"','"+descripcion+"','"+foto+"')"
        self.conn.escribir_db(query)

    def eliminar_producto(self,id):
        query = "DELETE FROM \"Productos\""+\
            "WHERE id="+str(id)
        self.conn.escribir_db(query)
    
       

    def insertar_credencial(self, usr, password, rol):
        query = 'INSERT INTO "Credenciales" (usr, password, rol)'+\
            "VALUES ('"+usr+"','"+password+"','"+rol+"')"
        self.conn.escribir_db(query)
        
    def close(self):
        self.conn.cerrar_db()
 
